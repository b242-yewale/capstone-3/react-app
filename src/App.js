import './App.css';
import { useState, useEffect } from 'react';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Routes } from 'react-router-dom';
import AppNavbar from './components/AppNavbar';
import { UserProvider } from './UserContext';
import Home from './pages/Home';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Register from './pages/Register';
import Products from './pages/Products';
import ProductView from './components/ProductView';
import Error from './pages/Error';
import Cart from './pages/Cart';
import OrderHistory from './pages/OrderHistory';
import Users from './pages/Users'

function App() {

  const [ user, setUser ] = useState({
    id: null,
    isAdmin: null,
    orders: [{
      product:[{
        productName: null,
        quantity: null,
        subTotal: null
      }],
      isCheckedOut: null,
      totalAmount: null,
    }]
  });

  const unsetUser = () => {
    localStorage.clear();
  };

  // check if the user information is properly stored upon login and the localStorage information is cleared upon logout
  useEffect(() => {
    console.log(user);
    console.log(localStorage);
  }, [user])

  return (
    <UserProvider value={{ user, setUser, unsetUser }}>
     <Router>
       <Container fluid>
         <AppNavbar/>
         <Routes>
           <Route path="/" element={<Home />} />
           <Route path="/products" element={<Products />} />
           <Route path="/products/:productId" element={<ProductView />} />
           <Route path="/logout" element={<Logout />} />
           <Route path="/register" element={<Register />} />
           <Route path="/login" element={<Login />} />
           <Route path="/cart" element={<Cart />} />
           <Route path="/getUserOrders" element={<OrderHistory />} />
           <Route path="/allUsers" element={<Users />} />
           <Route path="*" element={<Error />} />
         </Routes>
       </Container>
     </Router>
    </UserProvider>
  );
}

export default App;
