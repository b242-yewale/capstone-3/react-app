import { Row, Col, Card } from 'react-bootstrap';

export default function Highlights() {
	return (
		<Row>
			{/* 1st Card */}
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
			      <Card.Body>
			        <Card.Title>
			        	<h2>Buy products from accross the world</h2>
			        </Card.Title>
			        <Card.Text>
			          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
			        </Card.Text>
			      </Card.Body>
			    </Card>
			</Col>
			{/* 2nd Card */}
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
			      <Card.Body>
			        <Card.Title>
			        	<h2>Fastest home delivery</h2>
			        </Card.Title>
			        <Card.Text>
			          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
			        </Card.Text>
			      </Card.Body>
			    </Card>
			</Col>
			{/* 3rd Card */}
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
			      <Card.Body>
			        <Card.Title>
			        	<h2>Product guarantee and Consumer protection</h2>
			        </Card.Title>
			        <Card.Text>
			          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
			        </Card.Text>
			      </Card.Body>
			    </Card>
			</Col>
		</Row>
	)
}