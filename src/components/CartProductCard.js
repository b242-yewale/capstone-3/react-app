import PropTypes from 'prop-types';
import { Card } from 'react-bootstrap';

export default function CartProductCard({productProp}){

	console.log(productProp);

	const { productName, quantity, subTotal, _id } = productProp;
	console.log(_id);

	return (
	    <Card>
	        <Card.Body>
	            <Card.Title>{productName}</Card.Title>
	            <Card.Subtitle>Quantity:</Card.Subtitle>
	            <Card.Text>{quantity}</Card.Text>
	            <Card.Subtitle>subTotal:</Card.Subtitle>
	            <Card.Text>INR {subTotal}</Card.Text>
	        </Card.Body>
	    </Card>
	)	
}

CartProductCard.propTypes = {
    // The "shape" method is used to check if a prop object conforms to a specific "shape"
    cartProduct: PropTypes.shape({
        // Define the properties and their expected types
        productName: PropTypes.string.isRequired,
        quantity: PropTypes.number.isRequired,
        subTotal: PropTypes.number.isRequired
    })
}