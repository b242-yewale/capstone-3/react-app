import { Fragment, useState, useEffect } from 'react'
import ProductHistoryCard from "./ProductHistoryCard";

export default function UserOrderHistoryView({historyData}){

	const [history, setHistory] = useState([]);

    useEffect(() => {
        console.log(historyData)
        
        // Map through the courses received from the parent component (courses page) to render the course cards
        const productsHistoryArr = historyData.map(order => {
        	console.log(order)
            // Returns active courses as "CourseCard" components
				return (
					<ProductHistoryCard orderProp={order} key={order._id}/>
				)        	
        });

        // Set the "courses" state with the course card components returned by the map method
        // Allows the course card components to be rendered in this "UserView" component via the return statement below
        setHistory(productsHistoryArr);

    }, [historyData]);

    return(
        <Fragment>
            {history}
        </Fragment>
    );
}