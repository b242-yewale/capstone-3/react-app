import { useState, useEffect, useContext } from 'react';
import { useParams, useNavigate, Link } from 'react-router-dom';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function ProductView() {

	const { user } = useContext(UserContext);

	// allows us to gain access to methods that will allow us to redirect to a different page after enrolling to a course
	const navigate = useNavigate();

	// useParams hook allows us to retrieve the courseId passed via URL
	const { productId } = useParams();
	const { quantity } = useParams();

	const [ name, setName ] = useState("");
	const [ description, setDescription ] = useState("");
	const [ price, setPrice ] = useState(0);
	
	const addToCart = (productId) => {
		fetch('https://ecommerce-capstone-2.onrender.com/users/addToCart', {
			method: "POST",
			headers:{
				"Content-Type" : "application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				productId: productId,
				quantity: quantity
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);
			if (data === true){
				Swal.fire({
					title: "Product added to cart",
					icon: "success",
					text: "Continue shopping or checkout."
				})
				// navigate and redirect the user back to the courses page progammatically instead of using a component
				navigate("/products");
			} else {
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				})
			}
		})
	}

	useEffect(()=> {
		console.log(productId);

		fetch(`https://ecommerce-capstone-2.onrender.com/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data);
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
		})

	},[productId]);

	return(
		<Container className="mt-5">
			<Row>
				<Col lg={{ span: 6, offset: 3 }}>
					<Card>
					  <Card.Body className="text-center">
					    <Card.Title>{name}</Card.Title>
					    <Card.Subtitle>Description:</Card.Subtitle>
					    <Card.Text>{description}</Card.Text>
					    <Card.Subtitle>Price:</Card.Subtitle>
					    <Card.Text>PhP {price}</Card.Text>
					    {/*<Card.Subtitle>Class Schedule</Card.Subtitle>
					    <Card.Text>8 am - 5 pm</Card.Text>*/}
					    { (user.id !== null) ? 
					    	<Button variant="primary" block onClick={() => addToCart(productId)}>Add to cart</Button>
				    	:
					    	<Link className="btn btn-danger btn-block" to="/login" >Login and add to cart</Link>
				    	}
					  </Card.Body>		
					</Card>
				</Col>
			</Row>
		</Container>
	)
}