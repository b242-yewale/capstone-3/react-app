import PropTypes from 'prop-types';
import { Card } from 'react-bootstrap';

export default function CurrentTotalAmount({productProp}){

	console.log(productProp.totalAmount);

	const totalAmount = productProp.totalAmount;

	return (
	    <Card>
	        <Card.Body>
	            <Card.Subtitle>Total Amount:</Card.Subtitle>
	            <Card.Text>{totalAmount}</Card.Text>
	        </Card.Body>
	    </Card>
	)
}

CurrentTotalAmount.propTypes = {
    // The "shape" method is used to check if a prop object conforms to a specific "shape"
    currentAmount: PropTypes.shape({
        // Define the properties and their expected types
        
        Total: PropTypes.number.isRequired
    })
}