import PropTypes from 'prop-types';
import { Card } from 'react-bootstrap';
import { Fragment, useState, useEffect } from 'react'
import MultiOrderCard from "./MultiOrderCard";

export default function ProductHistoryCard({orderProp}){
	console.log(orderProp);

	const { product, isCheckedOut, totalAmount, purchasedOn, _id1 } = orderProp;
	console.log(product);
	console.log(isCheckedOut);
	let len = product.length;

	const[currProduct, setCurrProduct] = useState([]);

    useEffect(() => {
        console.log(product)
        
        // Map through the courses received from the parent component (courses page) to render the course cards
        const currProd = product.map(product => {
        	console.log(product)
            // Returns active courses as "CourseCard" components
				return (
					<MultiOrderCard prodProp={product} key={product._id}/>
				)        	
        });

        // Set the "courses" state with the course card components returned by the map method
        // Allows the course card components to be rendered in this "UserView" component via the return statement below
        setCurrProduct(currProd);

    }, [product]);

	return (
	    <Card>
	        <Card.Body>
	            <Card.Title>Total Amount</Card.Title>
	            <Card.Subtitle>{totalAmount}</Card.Subtitle>
	            <Card.Text>Order Checked out?</Card.Text>
	            <Card.Text>{isCheckedOut}</Card.Text>
	            <Card.Text>Order Date</Card.Text>
	            <Card.Subtitle>{purchasedOn}</Card.Subtitle>
	        </Card.Body>
	    </Card>
	)


	


}

ProductHistoryCard.propTypes = {
    // The "shape" method is used to check if a prop object conforms to a specific "shape"
    cartProduct: PropTypes.shape({
        // Define the properties and their expected types
        productName: PropTypes.string.isRequired,
        quantity: PropTypes.number.isRequired,
        subTotal: PropTypes.number.isRequired
    })
}