import { Fragment, useState, useEffect } from 'react';
import { Table, Button, Form } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function AdminUserView(props){

	const { usersData, fetchData } = props;

	console.log(usersData)

	// States for form inputs
	const [userId, setuserId] = useState("");
	const [users, setUsers] = useState([]);
	const [email, setEmail] = useState("");
	const [orders, setOrders] = useState([]);

	// States to open/close modals
	const [showEdit, setShowEdit] = useState(false);
	const [showAdd, setShowAdd] = useState(false);

	useEffect(() => {

		const archiveToggle = (userId, isAdmin) => {

			console.log(!isAdmin);

			fetch(`https://ecommerce-capstone-2.onrender.com/users/setAsAdmin/${userId}`, {
				method: 'POST',
				headers: {
					"Content-Type": "application/json",
					Authorization: `Bearer ${ localStorage.getItem('token') }`
				},
				body: JSON.stringify({
					isAdmin: !isAdmin
				})
			})
			.then(res => res.json())
			.then(data => {

				if (data === true) {

					fetchData();

					Swal.fire({
						title: "Success",
						icon: "success",
						text: "User successfully made admin/non-admin."
					});

				} else {

					fetchData();

					Swal.fire({
						title: "Something went wrong",
						icon: "error",
						text: "Please try again."
					});

				}
			})
		}

		const usersArr = usersData.map(user => {

			return(

				<tr key={user._id}>
					<td>{user.email}</td>
					<td>
						{/* 
							- If the course's "isActive" field is "true" displays "available"
							- Else if the course's "isActive" field is "false" displays "unavailable"
						*/}
						{user.isAdmin
							? <span>Admin</span>
							: <span>Non Admin</span>
						}
					</td>
					<td>
						{/* 
							- Display a red "Disable" button if course is "active"
							- Else display a green "Enable" button if course is "inactive"
						*/}
						{user.isAdmin
							?
							<Button 
								variant="danger" 
								size="sm" 
								onClick={() => archiveToggle(user._id, user.isAdmin)}
							>
								Make Non-Admin
							</Button>
							:
							<Button 
								variant="success"
								size="sm"
								onClick={() => archiveToggle(user._id, user.isAdmin)}
							>
								Make Admin
							</Button>
						}
					</td>
				</tr>

			)

		});

		// Set the "courses" state with the table rows returned by the map function
		// Renders table row elements inside the table via this "AdminView" return statement below
		setUsers(usersArr);

	}, [usersData, fetchData]);

	return(
		<Fragment>

			<div className="text-center my-4">
				<h2>Admin User Dashboard</h2>		
			</div>

			<Table striped bordered hover responsive>
				<thead className="bg-dark text-white">
					<tr>
						<th>Email</th>
						<th>Admin/ Non-Admin</th>
						<th>Actions</th>
					</tr>					
				</thead>
				<tbody>
					{users}
				</tbody>
			</Table>
			
		</Fragment>
	)
}