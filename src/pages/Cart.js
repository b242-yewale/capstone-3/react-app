import { useNavigate } from 'react-router-dom';
import { Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import { Fragment, useEffect, useState, useContext } from 'react';
import CartUserView from '../components/CartUserView';
import CurrentTotalAmount from '../components/CurrentTotalAmount';
import { Navigate } from 'react-router-dom';

export default function Cart() {

	const { user } = useContext(UserContext);

	const navigate = useNavigate();

	console.log(user)

	const [itemsInCart, setItemsInCart] = useState([]);

	//Function to fetch our courses data. The reason we have this in a function instead of directly in a useEffect hook is so that it can be reused and invoked ONLY when a page needs to re-render, instead of constantly (which causes a memory leak due to infinite looping)
	const fetchData = () => {

	    fetch(`https://ecommerce-capstone-2.onrender.com/users/cart`,{
	    	headers:{
				"Content-Type" : "application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
	    .then(res => res.json())
	    .then(data => {
	    	console.log(data)

	        setItemsInCart(data);

	    });

	}

	// Retrieves the courses from the database upon initial render of the "Courses" component
	useEffect(() => {
	    fetchData();
	}, []);

	const checkOut = () => {
		fetch('https://ecommerce-capstone-2.onrender.com/users/checkout', {
			method: "POST",
			headers:{
				"Content-Type" : "application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);
			if (data === true){
				Swal.fire({
					title: "User Checked Out",
					icon: "Success",
					text: "Shop with us again!"
				})
				// navigate and redirect the user back to the courses page progammatically instead of using a component
				navigate("/products");
			} else {
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				})
			}
		})
	}
	
	return (
	    <Fragment>
	    	{
	    		(user.isAdmin === false)?
		    		<Fragment>
		    			<CartUserView productsData={itemsInCart}/>
		    			<CurrentTotalAmount productProp={itemsInCart}/> 
		    			<Button variant="primary" block onClick={() => checkOut()}>Proceed to Checkout</Button>
		    		</Fragment>
		    		:
		    		<Navigate to='/' />
	    	}	    	      
	    </Fragment>
	)

}