import { Fragment, useEffect, useState, useContext } from 'react';
import AdminUserView from '../components/AdminUserView';
import UserContext from '../UserContext';

export default function Users(){

	const { user } = useContext(UserContext);

	const [users, setUsers] = useState([]);

	const fetchData = () => {

	    fetch(`https://ecommerce-capstone-2.onrender.com/users/allUsers`, {
	    	headers: {
	    		"Content-Type": "application/json",
	    		Authorization: `Bearer ${ localStorage.getItem('token') }`
	    	},
	    })
	    .then(res => res.json())
	    .then(data => {
	        console.log(data);
	        setUsers(data);

	    });

	}

	useEffect(() => {
	    fetchData();
	}, []);

	return (
	    <Fragment>
	    	<AdminUserView usersData={users} fetchData={fetchData}/>
	    </Fragment>
	)
}