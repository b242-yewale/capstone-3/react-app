import { Fragment, useEffect, useState, useContext } from 'react';
import AdminView from '../components/AdminView';
// import coursesData from '../data/coursesData';
import UserView from '../components/UserView';
import UserContext from '../UserContext';

export default function Products() {

    // Checks to see if the mock data was captured
    // console.log(coursesData);
    // console.log(coursesData[0]);

    const { user } = useContext(UserContext);

    // State that will be used to store the courses retrieved from the database
    const [products, setProducts] = useState([]);

    //Function to fetch our courses data. The reason we have this in a function instead of directly in a useEffect hook is so that it can be reused and invoked ONLY when a page needs to re-render, instead of constantly (which causes a memory leak due to infinite looping)
    const fetchData = () => {

        fetch(`https://ecommerce-capstone-2.onrender.com/products/all`)
        .then(res => res.json())
        .then(data => {
            console.log(data);
            setProducts(data);

        });

    }

    // Retrieves the courses from the database upon initial render of the "Courses" component
    useEffect(() => {
        fetchData();
    }, []);
    
    return (
        <Fragment>
            {/* If the user is an admin, show the Admin View. If not, show the regular courses page. */}
            {(user.isAdmin === true)
                ? <AdminView productsData={products} fetchData={fetchData}/>
                : <UserView productsData={products}/>
            }
        </Fragment>
    )

}