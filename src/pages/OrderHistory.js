import { useNavigate } from 'react-router-dom';
import { Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import { Fragment, useEffect, useState, useContext } from 'react';
import UserOrderHistoryView from '../components/UserOrderHistoryView';
import { Navigate } from 'react-router-dom';

export default function OrderHistory(){

	const { user } = useContext(UserContext);

	const navigate = useNavigate();

	console.log(user)

	const [orderHistory, setOrderHistory] = useState([]);

	const fetchData = () => {

	    fetch(`https://ecommerce-capstone-2.onrender.com/users/getUserOrders`,{
	    	headers:{
				"Content-Type" : "application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
	    .then(res => res.json())
	    .then(data => {
	    	console.log(data)

	        setOrderHistory(data);

	    });

	}

	// Retrieves the courses from the database upon initial render of the "Courses" component
	useEffect(() => {
	    fetchData();
	}, []);

	return (
	    <Fragment>
	    	{
	    		(user.isAdmin === false)?
		    		<Fragment>
		    			<UserOrderHistoryView historyData={orderHistory}/>
		    		</Fragment>
		    		:
		    		<Navigate to='/' />
	    	}	    	      
	    </Fragment>
	)
}