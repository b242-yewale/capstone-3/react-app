import { Fragment } from 'react';
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
import ProductCard from '../components/ProductCard';

export default function Home(){
    const data = {
        title: "E-Commerce Portal",
        content: "Get your favourite products at your doorstep",
        destination: "/products",
        label: "Buy now!"
    }

    return (
        <Fragment>
            <Banner data={data}/>
            <Highlights />
        </Fragment>
    )
}